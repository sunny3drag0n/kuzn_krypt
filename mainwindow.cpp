#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::func_1()
{
    if(buff_a == "" || buff_b == ""){ // проверка условий для инициализации
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте параметры эллептической кривой.")); // вывод ошибки
    }else{
        bignum_fromhex(EC.m, buff_p, ECCRYPT_BIGNUM_DIGITS);
        bignum_fromhex(EC.a, buff_a, ECCRYPT_BIGNUM_DIGITS);
        bignum_fromhex(EC.b, buff_b, ECCRYPT_BIGNUM_DIGITS);
        if(eccrypt_is_sing(EC)){
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Заданная элептическая кривая сингулярна, введите другие параметры.")); // вывод ошибки
        }
        else{
            QString str = "a="+QString(buff_a)+"\nb="+QString(buff_b)+"\np="+QString(buff_p)+"\nнесингулярна";
            QMessageBox::critical(NULL,QObject::tr("Задана элептическая кривая!"),tr(str.toUtf8().data())); // вывод ошибки

            buff_a=P_x.replace(" ","").toLatin1().data();
            bignum_fromhex(P.x, buff_a, ECCRYPT_BIGNUM_DIGITS);
            bignum_tohex(P.x, buff_a, BIGNUM_DIGITS(*P.x), ECCRYPT_BIGNUM_DIGITS);
            buff_b=P_y.replace(" ","").toLatin1().data();
            bignum_fromhex(P.y, buff_b, ECCRYPT_BIGNUM_DIGITS);
            bignum_tohex(P.y, buff_b, BIGNUM_DIGITS(*P.y), ECCRYPT_BIGNUM_DIGITS);
        }
    }

    A_k = rand() % 100 + 1;
    B_k = rand() % 100 + 1;
    func_2();
}

void MainWindow::func_2()//random kly4ey
{
    ui->A_k->setText(QString::number(A_k));
    ui->B_k->setText(QString::number(B_k));
}

// задать параметры кривой
void MainWindow::on_Elliptic_curve_param_accept_clicked()
{
    func_1();
}

// на стороне Алисы принять закрытый ключ
void MainWindow::on_A_k_accept_clicked()
{
    if((ui->A_k->text().replace(" ","")=="")){ // проверка условий
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте закрытый ключ Алисы!")); // вывод ошибки
        return;
    }else{
        A_k = ui->A_k->text().toInt(); // занесение значения ключа в память и вывод его в виджет
        ui->A_k_state->setText("Закрытый ключ Ka: "+ui->A_k->text());
        A_Qa = eccrypt_point_mul(P, ui->A_k->text().toInt(), EC);
        char buff[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/8];
        bignum_tohex(A_Qa.x, buff, BIGNUM_DIGITS(*A_Qa.x), ECCRYPT_BIGNUM_DIGITS);
        //ui->A_Qa_state->setText("Открытый ключ Qa: ("+QString(buff)+",\n");
        bignum_tohex(A_Qa.y, buff, BIGNUM_DIGITS(*A_Qa.y), ECCRYPT_BIGNUM_DIGITS);
        //ui->A_Qa_state->setText(ui->A_Qa_state->text()+QString(buff)+")");
    }
    on_A_send_Qa_Bob_clicked();
    if((ui->B_k->text().replace(" ","")=="")){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте закрытый ключ Боба!"));
        return;
    }else{
        B_k = ui->B_k->text().toInt();
        ui->B_k_state->setText("Закрытый ключ Kb: "+ui->B_k->text());
        B_Qb = eccrypt_point_mul(P, ui->B_k->text().toInt(), EC);
        char buff[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/8];
        bignum_tohex(B_Qb.x, buff, BIGNUM_DIGITS(*B_Qb.x), ECCRYPT_BIGNUM_DIGITS);
        //ui->B_Qb_state->setText("Открытый ключ Qb: ("+QString(buff)+",\n");
        bignum_tohex(B_Qb.y, buff, BIGNUM_DIGITS(*B_Qb.y), ECCRYPT_BIGNUM_DIGITS);
        //ui->B_Qb_state->setText(ui->B_Qb_state->text()+QString(buff)+")");
        on_B_send_Qb_Alice_clicked();
    }
}

// на стороне Боба принять закрытый ключ
void MainWindow::on_B_k_accept_clicked()
{
    if((ui->B_k->text().replace(" ","")=="")){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте закрытый ключ Алисы."));
    }else{
        B_k = ui->B_k->text().toInt();
        ui->B_k_state->setText("Закрытый ключ Kb: "+ui->B_k->text());
        B_Qb = eccrypt_point_mul(P, ui->B_k->text().toInt(), EC);
        char buff[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/8];
        bignum_tohex(B_Qb.x, buff, BIGNUM_DIGITS(*B_Qb.x), ECCRYPT_BIGNUM_DIGITS);
        //ui->B_Qb_state->setText("Открытый ключ Qb: ("+QString(buff)+",\n");
        bignum_tohex(B_Qb.y, buff, BIGNUM_DIGITS(*B_Qb.y), ECCRYPT_BIGNUM_DIGITS);
        //ui->B_Qb_state->setText(ui->B_Qb_state->text()+QString(buff)+")");
        on_B_send_Qb_Alice_clicked();
    }

}

// Алиса отправляет открытый ключ Бобу
void MainWindow::on_A_send_Qa_Bob_clicked()
{
    B_Qa = A_Qa; // передать значение Бобу и вывести в виджет
    char buff[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/8];
    bignum_tohex(B_Qa.x, buff, BIGNUM_DIGITS(*B_Qa.x), ECCRYPT_BIGNUM_DIGITS);
    ui->B_Qa_state->setText("Открытый ключ Алисы Qa: ("+QString(buff)+",\n");
    bignum_tohex(B_Qa.y, buff, BIGNUM_DIGITS(*B_Qa.y), ECCRYPT_BIGNUM_DIGITS);
    ui->B_Qa_state->setText(ui->B_Qa_state->text()+QString(buff)+")");

}

// Боб отправляет открытый ключ Алисе
void MainWindow::on_B_send_Qb_Alice_clicked()
{

    A_Qb = B_Qb;
    char buff[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/8];
    bignum_tohex(A_Qb.x, buff, BIGNUM_DIGITS(*A_Qb.x), ECCRYPT_BIGNUM_DIGITS);
    ui->A_Qb_state->setText("Открытый ключ Боба Qb: ("+QString(buff)+",\n");
    bignum_tohex(A_Qb.y, buff, BIGNUM_DIGITS(*A_Qb.y), ECCRYPT_BIGNUM_DIGITS);
    ui->A_Qb_state->setText(ui->A_Qb_state->text()+QString(buff)+")");

}

//вычисляет общий ключ
void MainWindow::on_A_calculate_Key_clicked()
{
    if((ui->A_Qb_state->text()=="Открытый ключ Боба Qb: не известен")||(ui->A_k_state->text()=="Мой закрытый ключ Ka: не задан")){ // проверка условий
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алисе не хватает данных для вычисления общего ключа.")); // вывод ошибки
    }else{
        A_Key = eccrypt_point_mul(A_Qb, A_k, EC); // вызов функции умножения точек
        qDebug() << BIGNUM_DIGIT_BITS<<BIGNUM_MAX_DIGITS<<BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/4;
        char *buff = new char[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/4]; // запись результата в качестве ключей ширования и дешифрования
        qDebug() << buff;
        bignum_tohex(A_Key.x, buff, BIGNUM_DIGITS(*A_Key.x), ECCRYPT_BIGNUM_DIGITS);
        ui->A_Key_state->setText("Общий ключ Ka*Qb: ("+QString(buff)+",\n");
        QString str = QString(buff);
        ui->A_Key_for_enc->setText("");
        while(str.length()!=0){
            if(str.length()<2){
                ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + "0");
                ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                str.remove(0,1);
            }else{
                if(str.length()==2){
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                }else{
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + " ");
                }
            }
        }
        bignum_tohex(A_Key.y, buff, BIGNUM_DIGITS(*A_Key.y), ECCRYPT_BIGNUM_DIGITS);
        ui->A_Key_state->setText(ui->A_Key_state->text()+QString(buff)+")");
        str.append(buff);
        ui->A_Key_for_dec->setText("");
        while(str.length()!=0){
            if(str.length()<2){
                ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + "0");
                ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                str.remove(0,1);
            }else{
                if(str.length()==2){
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                }else{
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + " ");
                }
            }
        }
    }
    on_B_calculate_Key_clicked();
}

// Боб  вычисляет общий ключ
void MainWindow::on_B_calculate_Key_clicked()
{
    if((ui->B_Qa_state->text()=="Открытый ключ Алисы Qa: не известен")||(ui->B_k_state->text()=="Мой закрытый ключ Kb: не задан")){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Бобу не хватает данных для вычисления общего ключа."));
    }else{
        B_Key = eccrypt_point_mul(B_Qa, B_k, EC);
        char *buff = new char[BIGNUM_DIGIT_BITS*BIGNUM_MAX_DIGITS/4]; // запись результата в качестве ключей ширования и дешифрования
        qDebug() << buff;
        bignum_tohex(B_Key.x, buff, BIGNUM_DIGITS(*B_Key.x), ECCRYPT_BIGNUM_DIGITS);
        ui->B_Key_state->setText("Общий ключ Kb*Qa: ("+QString(buff)+",\n");
        QString str = QString(buff);
        ui->A_Key_for_dec->setText("");
        while(str.length()!=0){
            if(str.length()<2){
                ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + "0");
                ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                str.remove(0,1);
            }else{
                if(str.length()==2){
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                }else{
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_dec->setText(ui->A_Key_for_dec->toPlainText() + " ");
                }
            }
        }
        bignum_tohex(B_Key.y, buff, BIGNUM_DIGITS(*B_Key.y), ECCRYPT_BIGNUM_DIGITS);
        ui->B_Key_state->setText(ui->B_Key_state->text()+QString(buff)+")");
        str.append(buff);
        ui->A_Key_for_enc->setText("");
        while(str.length()!=0){
            if(str.length()<2){
                ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + "0");
                ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                str.remove(0,1);
            }else{
                if(str.length()==2){
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                }else{
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + str.at(0));
                    str.remove(0,1);
                    ui->A_Key_for_enc->setText(ui->A_Key_for_enc->toPlainText() + " ");
                }
            }
        }
    }
}

// Алиса отправляет сообщение Бобу , т.е. сообщение шифруется у Алисы и дешифруется у Боба
void MainWindow::on_A_send_msg_clicked()
{
    // на стороне Алисы
    QStringList strk = ui->A_Key_for_enc->toPlainText().split(" ", QString::SkipEmptyParts); // берем ключ
    if (strk.count()>32){
        while(strk.count()!=32){
            strk.removeLast();
        }
    }
    while(strk.length()!=32){
        strk.append("00");
    }
    QStringList strtext = ui->A_msg_for_Bob->toPlainText().split(" ", QString::SkipEmptyParts); // берем текст
    if(strtext.count()==0){
        strtext.append("00");
    }
    if(((strtext.count()%BLOCK_SIZE)!=0)){
        while((strtext.count()%BLOCK_SIZE)!=0){
            strtext.append("00");
        }
    }
    ui->Last_ciphertext->setText(""); // очищаем виджет с шифротекстом
    uint8_t k[BLOCK_SIZE*2];
    for(int i = 0 ; i < BLOCK_SIZE*2 ; i++) // конверитруем ключ
    {
        QByteArray ba = strk.at(i).toLatin1();
        const char *c_str = ba.data();
        k[i] = strtol(c_str,NULL,16);
    }
    while (strtext.count()!=0){ // пока есть текст который надо зашифровать
        uint8_t a[BLOCK_SIZE];
        for(int i = 0 ; i < BLOCK_SIZE ; i++) // конвертируем блок текста
        {
            QByteArray ba = strtext.at(i).toLatin1();
            const char *c_str = ba.data();
            a[i] = strtol(c_str,NULL,16);
        }
        kuznyechik_encrypt(k, a, a); // передаем ключ и блок текста в функцию шифрования
        for(int i = 0 ; i < BLOCK_SIZE ; i++){ // пишем результат в виджет последнего зашифрованного сообщения (передаем в канал связи)
            QString strout="";
            char buffer [3];

            itoa (a[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }
            strout.append(" ");
            ui->Last_ciphertext->setText(ui->Last_ciphertext->toPlainText() + strout);
        }
        for(int i = 0 ; i < BLOCK_SIZE ; i ++)
        {
            strtext.removeFirst();
        }
    }
    //На стороне Боба
    strk = ui->A_Key_for_dec->toPlainText().split(" ", QString::SkipEmptyParts); // берем ключ дешифрации
    if (strk.count()>32){
        while(strk.count()!=32){
            strk.removeLast();
        }
    }
    while(strk.length()!=32){
        strk.append("00");
    }
    strtext = ui->Last_ciphertext->toPlainText().split(" ", QString::SkipEmptyParts); // берем шифро текст из виджета (канала связи)
    if(strtext.count()==0){
        strtext.append("00");
    }
    if(((strtext.count()%BLOCK_SIZE)!=0)){
        while((strtext.count()%BLOCK_SIZE)!=0){
            strtext.append("00");
        }
    }
    ui->A_msg_form_Bob->setText(""); // очищаем виджет последнего сообщения от Алисы
    for(int i = 0 ; i < BLOCK_SIZE*2 ; i++)
    {
        QByteArray ba = strk.at(i).toLatin1();
        const char *c_str = ba.data();
        k[i] = strtol(c_str,NULL,16);
    }
    while (strtext.count()!=0){ // пока шифро текст не закончился
        uint8_t a[BLOCK_SIZE];
        for(int i = 0 ; i < BLOCK_SIZE ; i++) // берем его блок
        {
            QByteArray ba = strtext.at(i).toLatin1();
            const char *c_str = ba.data();
            a[i] = strtol(c_str,NULL,16);
        }
        kuznyechik_decrypt(k, a, a); // передаем его в функцию в дкшифрации использую ключ дешифрации Боба
        for(int i = 0 ; i < BLOCK_SIZE ; i++){ // результат пишем в виджет на стороне Боба

            QString strout="";
            char buffer [3];

            itoa (a[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }

            strout.append(" ");
            ui->A_msg_form_Bob->setText(ui->A_msg_form_Bob->toPlainText() + strout);
        }
        for(int i = 0 ; i < BLOCK_SIZE ; i ++)
        {
            strtext.removeFirst();
        }
    }
}

// Боб отправляет сообщение Алисе
void MainWindow::on_B_send_msg_clicked()
{
    QStringList strk = ui->A_Key_for_enc->toPlainText().split(" ", QString::SkipEmptyParts);
    if (strk.count()>32){
        while(strk.count()!=32){
            strk.removeLast();
        }
    }
    while(strk.length()!=32){
        strk.append("00");
    }
    QStringList strtext = ui->A_msg_for_Bob->toPlainText().split(" ", QString::SkipEmptyParts);
    if(strtext.count()==0){
        strtext.append("00");
    }
    if(((strtext.count()%BLOCK_SIZE)!=0)){
        while((strtext.count()%BLOCK_SIZE)!=0){
            strtext.append("00");
        }
    }
    ui->Last_ciphertext->setText("");
    uint8_t k[BLOCK_SIZE*2];
    for(int i = 0 ; i < BLOCK_SIZE*2 ; i++)
    {
        QByteArray ba = strk.at(i).toLatin1();
        const char *c_str = ba.data();
        k[i] = strtol(c_str,NULL,16);
    }
    while (strtext.count()!=0){
        uint8_t a[BLOCK_SIZE];
        for(int i = 0 ; i < BLOCK_SIZE ; i++)
        {
            QByteArray ba = strtext.at(i).toLatin1();
            const char *c_str = ba.data();
            a[i] = strtol(c_str,NULL,16);
        }
        kuznyechik_encrypt(k, a, a);
        for(int i = 0 ; i < BLOCK_SIZE ; i++){
            QString strout="";
            char buffer [3];

            itoa (a[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }
            strout.append(" ");
            ui->Last_ciphertext->setText(ui->Last_ciphertext->toPlainText() + strout);
        }
        for(int i = 0 ; i < BLOCK_SIZE ; i ++)
        {
            strtext.removeFirst();
        }
    }
    strk = ui->A_Key_for_dec->toPlainText().split(" ", QString::SkipEmptyParts);
    if (strk.count()>32){
        while(strk.count()!=32){
            strk.removeLast();
        }
    }
    while(strk.length()!=32){
        strk.append("00");
    }
    strtext = ui->Last_ciphertext->toPlainText().split(" ", QString::SkipEmptyParts);
    if(strtext.count()==0){
        strtext.append("00");
    }
    if(((strtext.count()%BLOCK_SIZE)!=0)){
        while((strtext.count()%BLOCK_SIZE)!=0){
            strtext.append("00");
        }
    }
    ui->A_msg_form_Bob->setText("");
    for(int i = 0 ; i < BLOCK_SIZE*2 ; i++)
    {
        QByteArray ba = strk.at(i).toLatin1();
        const char *c_str = ba.data();
        k[i] = strtol(c_str,NULL,16);
    }
    while (strtext.count()!=0){
        uint8_t a[BLOCK_SIZE];
        for(int i = 0 ; i < BLOCK_SIZE ; i++)
        {
            QByteArray ba = strtext.at(i).toLatin1();
            const char *c_str = ba.data();
            a[i] = strtol(c_str,NULL,16);
        }
        kuznyechik_decrypt(k, a, a);
        for(int i = 0 ; i < BLOCK_SIZE ; i++){

            QString strout="";
            char buffer [3];

            itoa (a[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }

            strout.append(" ");
            ui->A_msg_form_Bob->setText(ui->A_msg_form_Bob->toPlainText() + strout);
        }
        for(int i = 0 ; i < BLOCK_SIZE ; i ++)
        {
            strtext.removeFirst();
        }
    }
}

